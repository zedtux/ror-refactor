## Unreleased

## 0.3.1 - (2016-11-25)

* Adds a configuration field in order to set the created services path ([#1](https://github.com/zedtux/ror-refactor/issues/1))
* Adds a configuration field in order to the name of a module to be included in created services ([#1](https://github.com/zedtux/ror-refactor/issues/1))
* Handle namespaces in service class names ([#1](https://github.com/zedtux/ror-refactor/issues/1))

## 0.3.0 - (2016-08-24)

* Adds the new refactoring tool "Extract Service"

## 0.2.0 - (2016-08-16)

* Extract Method - Move code in a class
* Adds the new refactoring tool "Extract Line"

## 0.1.2 - (2016-08-12)

* Extract Method - Fixes cursors positions

## 0.1.1 - (2016-08-11)

* Extract Method - Fix case with code on top and no space
* Add tests

## 0.1.0 - (2016-08-10)

* Every feature added
* Every bug fixed
